cmake_minimum_required(VERSION 3.16)
project(motion_detection)
set(OUTPUT_NAME "motion_detection")

set(CMAKE_CXX_STANDARD 17)

find_package(OpenCV REQUIRED)
find_package(OpenMP REQUIRED)
if (OPENMP_FOUND)
    set (CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${OpenMP_C_FLAGS}")
    set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${OpenMP_CXX_FLAGS}")
    set (CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} ${OpenMP_EXE_LINKER_FLAGS}")
endif()

file(GLOB_RECURSE HEADER_FILES src/*.hpp)
file(GLOB_RECURSE SRC_FILES src/*.cpp)

include_directories(${OpenCV_INCLUDE_DIRS})

add_executable(${OUTPUT_NAME} ${SRC_FILES} main.cpp ${HEADER_FILES})
target_link_libraries(${OUTPUT_NAME} ${OpenCV_LIBS})
target_compile_options(${OUTPUT_NAME} PRIVATE -W -Wall -Werror -fopenmp -O0)
