#!/bin/bash
# motion_detection [video] [nb_threads] [width] [height] [version]

echo "v_opencv_640_480"
./build/motion_detection 0 1 640 480 -1 > results/v_opencv_640_480_optim_0.txt
echo "v_opencv_1280_720"
./build/motion_detection 0 1 1280 720 -1 > results/v_opencv_1280_720_optim_0.txt
echo "v_opencv_1920_1080"
./build/motion_detection 0 1 1920 1080 -1 > results/v_opencv_1920_1080_optim_0.txt

echo "v_0_640_480"
./build/motion_detection 0 1 640 480 0 > results/v_0_640_480_optim_0.txt
echo "v_0_1280_720"
./build/motion_detection 0 1 1280 720 0 > results/v_0_1280_720_optim_0.txt
echo "v_0_1920_1080"
./build/motion_detection 0 1 1920 1080 0 > results/v_0_1920_1080_optim_0.txt

echo "v_1_640_480_1_threads"
./build/motion_detection 0 1 640 480 1 > results/v_1_640_480_1_threads_optim_0.txt
echo "v_1_640_480_2_threads"
./build/motion_detection 0 2 640 480 1 > results/v_1_640_480_2_threads_optim_0.txt
echo "v_1_640_480_3_threads"
./build/motion_detection 0 3 640 480 1 > results/v_1_640_480_3_threads_optim_0.txt
echo "v_1_640_480_4_threads"
./build/motion_detection 0 4 640 480 1 > results/v_1_640_480_4_threads_optim_0.txt
echo "v_1_640_480_5_threads"
./build/motion_detection 0 5 640 480 1 > results/v_1_640_480_5_threads_optim_0.txt
echo "v_1_640_480_6_threads"
./build/motion_detection 0 6 640 480 1 > results/v_1_640_480_6_threads_optim_0.txt
echo "v_1_640_480_7_threads"
./build/motion_detection 0 7 640 480 1 > results/v_1_640_480_7_threads_optim_0.txt
echo "v_1_640_480_8_threads"
./build/motion_detection 0 8 640 480 1 > results/v_1_640_480_8_threads_optim_0.txt

echo "v_1_1280_720_1_threads"
./build/motion_detection 0 1 1280 720 1 > results/v_1_1280_720_1_threads_optim_0.txt
echo "v_1_1280_720_2_threads"
./build/motion_detection 0 2 1280 720 1 > results/v_1_1280_720_2_threads_optim_0.txt
echo "v_1_1280_720_3_threads"
./build/motion_detection 0 3 1280 720 1 > results/v_1_1280_720_3_threads_optim_0.txt
echo "v_1_1280_720_4_threads"
./build/motion_detection 0 4 1280 720 1 > results/v_1_1280_720_4_threads_optim_0.txt
echo "v_1_1280_720_5_threads"
./build/motion_detection 0 5 1280 720 1 > results/v_1_1280_720_5_threads_optim_0.txt
echo "v_1_1280_720_6_threads"
./build/motion_detection 0 6 1280 720 1 > results/v_1_1280_720_6_threads_optim_0.txt
echo "v_1_1280_720_7_threads"
./build/motion_detection 0 7 1280 720 1 > results/v_1_1280_720_7_threads_optim_0.txt
echo "v_1_1280_720_8_threads"
./build/motion_detection 0 8 1280 720 1 > results/v_1_1280_720_8_threads_optim_0.txt

echo "v_1_1920_1080_1_threads"
./build/motion_detection 0 1 1920 1080 1 > results/v_1_1920_1080_1_threads_optim_0.txt
echo "v_1_1920_1080_2_threads"
./build/motion_detection 0 2 1920 1080 1 > results/v_1_1920_1080_2_threads_optim_0.txt
echo "v_1_1920_1080_3_threads"
./build/motion_detection 0 3 1920 1080 1 > results/v_1_1920_1080_3_threads_optim_0.txt
echo "v_1_1920_1080_4_threads"
./build/motion_detection 0 4 1920 1080 1 > results/v_1_1920_1080_4_threads_optim_0.txt
echo "v_1_1920_1080_5_threads"
./build/motion_detection 0 5 1920 1080 1 > results/v_1_1920_1080_5_threads_optim_0.txt
echo "v_1_1920_1080_6_threads"
./build/motion_detection 0 6 1920 1080 1 > results/v_1_1920_1080_6_threads_optim_0.txt
echo "v_1_1920_1080_7_threads"
./build/motion_detection 0 7 1920 1080 1 > results/v_1_1920_1080_7_threads_optim_0.txt
echo "v_1_1920_1080_8_threads"
./build/motion_detection 0 8 1920 1080 1 > results/v_1_1920_1080_8_threads_optim_0.txt

echo "v_2_640_480_1_threads"
./build/motion_detection 0 1 640 480 2 > results/v_2_640_480_1_threads_optim_0.txt
echo "v_2_640_480_2_threads"
./build/motion_detection 0 2 640 480 2 > results/v_2_640_480_2_threads_optim_0.txt
echo "v_2_640_480_3_threads"
./build/motion_detection 0 3 640 480 2 > results/v_2_640_480_3_threads_optim_0.txt
echo "v_2_640_480_4_threads"
./build/motion_detection 0 4 640 480 2 > results/v_2_640_480_4_threads_optim_0.txt
echo "v_2_640_480_5_threads"
./build/motion_detection 0 5 640 480 2 > results/v_2_640_480_5_threads_optim_0.txt
echo "v_2_640_480_6_threads"
./build/motion_detection 0 6 640 480 2 > results/v_2_640_480_6_threads_optim_0.txt
echo "v_2_640_480_7_threads"
./build/motion_detection 0 7 640 480 2 > results/v_2_640_480_7_threads_optim_0.txt
echo "v_2_640_480_8_threads"
./build/motion_detection 0 8 640 480 2 > results/v_2_640_480_8_threads_optim_0.txt

echo "v_2_1280_720_1_threads"
./build/motion_detection 0 1 1280 720 2 > results/v_2_1280_720_1_threads_optim_0.txt
echo "v_2_1280_720_2_threads"
./build/motion_detection 0 2 1280 720 2 > results/v_2_1280_720_2_threads_optim_0.txt
echo "v_2_1280_720_3_threads"
./build/motion_detection 0 3 1280 720 2 > results/v_2_1280_720_3_threads_optim_0.txt
echo "v_2_1280_720_4_threads"
./build/motion_detection 0 4 1280 720 2 > results/v_2_1280_720_4_threads_optim_0.txt
echo "v_2_1280_720_5_threads"
./build/motion_detection 0 5 1280 720 2 > results/v_2_1280_720_5_threads_optim_0.txt
echo "v_2_1280_720_6_threads"
./build/motion_detection 0 6 1280 720 2 > results/v_2_1280_720_6_threads_optim_0.txt
echo "v_2_1280_720_7_threads"
./build/motion_detection 0 7 1280 720 2 > results/v_2_1280_720_7_threads_optim_0.txt
echo "v_2_1280_720_8_threads"
./build/motion_detection 0 8 1280 720 2 > results/v_2_1280_720_8_threads_optim_0.txt

echo "v_2_1920_1080_1_threads"
./build/motion_detection 0 1 1920 1080 2 > results/v_2_1920_1080_1_threads_optim_0.txt
echo "v_2_1920_1080_2_threads"
./build/motion_detection 0 2 1920 1080 2 > results/v_2_1920_1080_2_threads_optim_0.txt
echo "v_2_1920_1080_3_threads"
./build/motion_detection 0 3 1920 1080 2 > results/v_2_1920_1080_3_threads_optim_0.txt
echo "v_2_1920_1080_4_threads"
./build/motion_detection 0 4 1920 1080 2 > results/v_2_1920_1080_4_threads_optim_0.txt
echo "v_2_1920_1080_5_threads"
./build/motion_detection 0 5 1920 1080 2 > results/v_2_1920_1080_5_threads_optim_0.txt
echo "v_2_1920_1080_6_threads"
./build/motion_detection 0 6 1920 1080 2 > results/v_2_1920_1080_6_threads_optim_0.txt
echo "v_2_1920_1080_7_threads"
./build/motion_detection 0 7 1920 1080 2 > results/v_2_1920_1080_7_threads_optim_0.txt
echo "v_2_1920_1080_8_threads"
./build/motion_detection 0 8 1920 1080 2 > results/v_2_1920_1080_8_threads_optim_0.txt