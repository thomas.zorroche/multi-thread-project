#include <opencv2/opencv.hpp>
#include <opencv2/highgui.hpp>
#include <iostream>
#include <string>
#include <omp.h>

// #define SHOW_FRAMES

enum Key { ONE = 49, ESC = 27, A = 97, B = 98, C = 99, E = 101, R = 114, S = 115, Z = 122 };
enum Version { V_OPENCV = -1, V0 = 0, V1 = 1, V2 = 2};

struct camParameter {
    double step;
    int min;
    int max;
    double value;
    bool available;
};

void naiveSubtractBackground(cv::Mat& dst, cv::Mat& frame, cv::Mat& background);
void subtractBackground(cv::Mat& dst, cv::Mat& frame, cv::Mat& background);
void subtract(int id_start, int size, uchar *ptr_dst, uchar *ptr_frame, uchar *ptr_background);
void subtractBackgroundSIMD(cv::Mat& dst, cv::Mat& frame, cv::Mat& background);
void subtractSIMD(int id_start, int size, uchar *ptr_dst, uchar *ptr_frame, uchar *ptr_background);

static int NUM_THREADS;
static int WIDTH;
static int HEIGHT;
static int VERSION;

int main(int argc, char const *argv[])
{
    // Check if the user gave a camera identifier
    if(argc <= 1) {
        std::cerr << "Error, please provide a video input index." << std::endl;
        std::cout << std::system("v4l2-ctl --list-devices") << std::endl;
        std::cout << std::system("v4l2-ctl -d /dev/video1 --list-formats-ext") << std::endl;
        return -1;
    }
    const int index = std::atoi(argv[1]);

    NUM_THREADS = std::atoi(argv[2]);
    std::cout << "NB_THREADS = " << NUM_THREADS << std::endl;

    WIDTH = std::atoi(argv[3]);
    HEIGHT = std::atoi(argv[4]);
    std::cout << "SIZE = " << WIDTH << "x" << HEIGHT << std::endl;

    VERSION = std::atoi(argv[5]);
    std::cout << "VERSION = " << VERSION << std::endl;

    cv::VideoCapture video;
    cv::Mat frame = cv::Mat(WIDTH, HEIGHT, CV_32F);
    cv::Mat computing_frame = cv::Mat(WIDTH, HEIGHT, CV_32F);
    cv::Mat frame_resized = cv::Mat(WIDTH, HEIGHT, CV_32FC3);

    // Setup video capture
    video = cv::VideoCapture(index, cv::CAP_V4L);
    if(!video.isOpened()) {
        std::cerr << "Error opening video flux." << std::endl;
        return false;
    }

    // OMP setup
    if(VERSION == V1 || VERSION == V2)
        omp_set_num_threads(NUM_THREADS);

    // Beginning of the program
#ifdef SHOW_FRAMES
    int keyPressed;
#endif
    double start_time = 0.0;
    double end_time = 0.0;
    double start_time_subtract = 0.0;
    double elapsed_time_subtract = 0.0;
    double elapsed_time = 0;
    int nbFramesComputeBackground = 1;
    int nbFrames = 1000;
    cv::Mat background = cv::Mat(WIDTH, HEIGHT, CV_32F);
    cv::Mat frame_temp = cv::Mat(HEIGHT, WIDTH, CV_8U); // Don't know why, but WIDTH and HEIGHT must be reversed...
    cv::Mat display_frame = cv::Mat(WIDTH, HEIGHT, CV_8U);
    cv::Mat kernel = cv::Mat(200, 60, CV_8U, cv::Scalar(1,1,1));
    std::cout << "---------- BEG PROGRAM ----------" << std::endl;
    std::cout << "Number of frames to compute the background model: " << nbFramesComputeBackground << std::endl;
    std::cout << "Number of frames: " << nbFrames << std::endl;

    double start_global_time = omp_get_wtime();
    // Get first frame
    if(!video.read(frame)) {
        std::cerr << "Application::run : Error reading the current frame." << std::endl;
        return -1;
    }
    cv::resize(frame, frame_resized, cv::Size(WIDTH, HEIGHT));
    cv::cvtColor(frame_resized, computing_frame, cv::COLOR_RGB2GRAY);

    cv::Mat clone = computing_frame.clone();
    clone.convertTo(clone, CV_32F);
    background = clone;

    // Compute background model
    for(int frame_index = 0; frame_index < nbFramesComputeBackground; ++frame_index) {
        if(!video.read(frame)) {
            std::cerr << "Application::run : Error reading the current frame." << std::endl;
            return -1;
        }
        cv::resize(frame, frame_resized, cv::Size(WIDTH, HEIGHT));
        cv::cvtColor(frame_resized, computing_frame, cv::COLOR_RGB2GRAY);
        
        cv::Mat clone = computing_frame.clone();
        clone.convertTo(clone, CV_32F);
        background += clone;
    }

    background /= (float)nbFramesComputeBackground;
    background.convertTo(background, CV_8U);

    #ifdef SHOW_FRAMES
        cv::imshow("Background", background);
    #endif

    start_time = omp_get_wtime();
    for(int frame_index = 0; frame_index < nbFrames; ++frame_index) {
        video.read(frame);
        cv::resize(frame, frame_resized, cv::Size(WIDTH, HEIGHT));
        cv::cvtColor(frame_resized, computing_frame, cv::COLOR_RGB2GRAY);
        computing_frame.convertTo(computing_frame, CV_8U);
#ifdef SHOW_FRAMES
        cv::imshow("Raw output", computing_frame);
#endif

        // https://docs.opencv.org/3.4/d9/d61/tutorial_py_morphological_ops.html
        
        switch (VERSION) {
            case V0:
                start_time_subtract = omp_get_wtime();
                naiveSubtractBackground(frame_temp, computing_frame, background);
                elapsed_time_subtract += omp_get_wtime() - start_time_subtract;
                break;

            case V1:
                start_time_subtract = omp_get_wtime();
                subtractBackground(frame_temp, computing_frame, background);
                elapsed_time_subtract += omp_get_wtime() - start_time_subtract;
                break;

            case V2:
                start_time_subtract = omp_get_wtime();
                subtractBackgroundSIMD(frame_temp, computing_frame, background);
                elapsed_time_subtract += omp_get_wtime() - start_time_subtract;
                break;

            case V_OPENCV:
                start_time_subtract = omp_get_wtime();
                frame_temp = cv::abs(computing_frame - background);
                elapsed_time_subtract += omp_get_wtime() - start_time_subtract;
                break;
            
            default:
                std::cerr << "VERSION NOT DEFINED: " << VERSION << std::endl;
                break;
        }
        cv::threshold(frame_temp, frame_temp, 25, 255, cv::THRESH_BINARY);
        cv::morphologyEx(frame_temp, display_frame, cv::MORPH_OPEN, kernel);

#ifdef SHOW_FRAMES
        // cv::rotate(display_frame, display_frame, cv::ROTATE_90_CLOCKWISE);
        cv::imshow("Post process", display_frame);
        keyPressed = cv::waitKey(16.6667); // 1000 / 60 = 16.6667
        switch (keyPressed)
        {
            case Key::ESC:
                return 0;
                break;
                
            default:
                break;
        }
#endif
    }
    end_time = omp_get_wtime();

    std::cout << "---------- END PROGRAM ----------" << std::endl;
    elapsed_time = omp_get_wtime() - start_global_time;
    std::cout << "Elapsed time (global): " << std::setprecision(4) << elapsed_time << "s" << std::endl;

    std::cout << "---------------------------------" << std::endl;
    elapsed_time = end_time - start_time;
    std::cout << "Elapsed time (total post process): " << std::setprecision(4) << elapsed_time << "s" << std::endl;
    std::cout << "Time per frame: " << std::setprecision(16) << elapsed_time / (double)nbFrames * 1000 << "ms" << std::endl;

    std::cout << "Elapsed time (total subtract background): " << std::setprecision(4) << elapsed_time_subtract << "s" << std::endl;
    std::cout << "Time per frame: " << std::setprecision(16) << elapsed_time_subtract / (double)nbFrames * 1000 << "ms" << std::endl;
    std::cout << std::endl;

    return 0;
}

void subtract(int id_start, int size, uchar *ptr_dst, uchar *ptr_frame, uchar *ptr_background) {
    int id_value = 0;
    for(int id = 0; id < size; ++id) { // image data structure: [R, G, B, R, G, B, R, G, B, ...]
        id_value = id_start + id;
        ptr_dst[id_value] = std::abs(ptr_frame[id_value] - ptr_background[id_value]);
    }
}

void subtractBackground(cv::Mat& dst, cv::Mat& frame, cv::Mat& background) {
    uchar *ptr_dst = dst.data;
    uchar *ptr_frame = frame.data;
    uchar *ptr_background = background.data;
    int id_thread = 0;
    int size = frame.rows * frame.cols;
    int size_per_thread = (float)size / NUM_THREADS;
    int id_start = 0;
    
    #pragma omp parallel shared(size_per_thread, ptr_dst, ptr_frame, ptr_background) private(id_thread, id_start) num_threads(NUM_THREADS)
    {
        id_thread = omp_get_thread_num();
        id_start = size_per_thread * id_thread;
        subtract(id_start, size_per_thread, ptr_dst, ptr_frame, ptr_background);
    }
}

void naiveSubtractBackground(cv::Mat& dst, cv::Mat& frame, cv::Mat& background) {
    uchar *ptr_dst = dst.data;
    uchar *ptr_frame = frame.data;
    uchar *ptr_background = background.data;
    const int size = frame.rows * frame.cols;

    for(int id = 0; id < size; ++id) { // image data structure: [R, G, B, R, G, B, R, G, B, ...]
        ptr_dst[id] = ptr_frame[id] - ptr_background[id];
    }
}

void subtractSIMD(int id_start, int size, uchar *ptr_dst, uchar *ptr_frame, uchar *ptr_background) {
    int id_value = 0;
    #pragma omp simd
    for(int id = 0; id < size; ++id) { // image data structure: [R, G, B, R, G, B, R, G, B, ...]
        id_value = id_start + id;
        ptr_dst[id_value] = std::abs(ptr_frame[id_value] - ptr_background[id_value]);
    }
}

void subtractBackgroundSIMD(cv::Mat& dst, cv::Mat& frame, cv::Mat& background) {
    uchar *ptr_dst = dst.data;
    uchar *ptr_frame = frame.data;
    uchar *ptr_background = background.data;
    int id_thread = 0;
    int size = frame.rows * frame.cols;
    int size_per_thread = (float)size / NUM_THREADS;
    int id_start = 0;
    
    #pragma omp parallel shared(size_per_thread, ptr_dst, ptr_frame, ptr_background) private(id_thread, id_start) num_threads(NUM_THREADS)
    {
        id_thread = omp_get_thread_num();
        id_start = size_per_thread * id_thread;
        subtractSIMD(id_start, size_per_thread, ptr_dst, ptr_frame, ptr_background);
    }
}