# Motion detection

## Dependancies
* **opencv >= 4.2**

  Install: `sudo apt install libopencv-dev`<br>
  In some cases, you may need to move the opencv2 folder from`/usr/include/opencv4/` to `/usr/include/`.<br>
  Use: `sudo mv /usr/include/opencv4/opencv2/ /usr/include/`
* **cmake >= 3.16**

## Build
* Clone the project
* Go in the project folder and create a `build` directory
* Go in the `build` directory and use: `cmake ..`
* Make: `make -j4`

## Run
* In the `build` directory type: `./motion-detection`