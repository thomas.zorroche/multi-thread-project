from genericpath import isfile
from operator import contains
import os

definitions = ["640_480", "1280_720", "1920_1080"]
folders = ["./results/optim_0/", "./results/optim_1/", "./results/optim_2/"]
outputs = ["Optim_0.txt", "Optim_1.txt", "Optim_2.txt"]

output_index = 0
for folder in folders:
    output = open(outputs[output_index], "w")
    files = [f for f in os.listdir(folder) if isfile(os.path.join(folder, f))]
    files.sort()

    for definition in definitions:
        files_current_def = [f for f in files if definition in f]
        for file in files_current_def:
            data = open(folder + file, "r")
            lines = data.readlines()
            infos = file.split(".txt")[0] + ": " + lines[-2].split(" ")[-1].split("ms")[0]
            output.write(infos + "\n")
            data.close()
        output.write("\n")

    output_index += 1

output.close()